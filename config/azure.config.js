module.exports = {
    key1: '95b51da541c54d4fb7d071f4fac4919b',
    key2: 'f08617d35bd34cf0a7968d3127256790',
    api: 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/',
    detectEndpoint: 'detect/',
    verifyEndpoint: 'verify/',
    serviceHost: 'http://40.68.220.195:3000/'
};