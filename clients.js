var clients = [];

module.exports = {
    addClient: function (client) {
        clients.push(client);
    },
    removeClient: function (client) {
        clients = clients.filter(function (existingClient) {
            return existingClient.id !== client.id;
        });
    },
   broadcastEmit: function (emit, payload) {
        clients.forEach(function (client) {
            client.emit(emit, payload);
        })
    }
};