var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var fs = require('fs');
var azureConfig = require('../config/azure.config');
var doh1Config = require('../config/doh1.config');
var clients = require('../clients');
var multer = require('multer');
var path = require('path');
var uploadPath = path.resolve(__dirname, '../public/uploads/');
var upload = multer({
    dest: uploadPath,
});
var Jimp = require('jimp');
var exec = require('child_process').exec;

router.post('/', upload.fields([
    { name: 'image', maxCount: 1 }
]), function (req, res, next) {
    var payload = {};
    Jimp.read(uploadPath + '/' + req.files.image[0].filename).then(function (image) {
        return image
            .resize(image.bitmap.width * 0.5, image.bitmap.height * 0.5)
            .write(uploadPath + '/' + req.files.image[0].filename + '.jpeg')
    })
        .then(function (result) {
            clients.broadcastEmit('log', 'lpring');
            return new Promise(function (resolve, reject) {
                var lprPath = path.resolve(__dirname, '..\\LPR\\LPR.exe');
                var lprArg = path.resolve(uploadPath + '/' + req.files.image[0].filename + '.jpeg');
                clients.broadcastEmit('log', `start "" /W /B "${lprPath}" "${lprArg}"`);
                exec(`start "" /W /B "${lprPath}" "${lprArg}"`,
                    function (error, stdout, stderr) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(stdout);
                        }
                    });
            });
        })
        .then(function (licensePlate) {
            clients.broadcastEmit('log', `licensePlate1: ${licensePlate}`);
            var result = /\d{5,}/.exec(licensePlate);
            if (result && result.length && result.length > 0) {
                result = result[0];
                clients.broadcastEmit('log', `licensePlate2: ${result}`);
                console.log(`licensePlate: ${result}`);
                return rp({
                    method: 'GET',
                    uri: doh1Config.api + doh1Config.licensePlateEndpoint + result,
                    json: true
                });
            } else {
                result = 'NO LICENSE DETECTED';
                throw {
                    message: result,
                    statusCode: 500
                };
            }
        })
        .then(function (result) {
            clients.broadcastEmit('log', 'detecting');
            Object.assign(payload, {
                firstName: result.FIRST_NAME,
                lastName: result.LAST_NAME,
                cellphoneNumber: result.CELL_PHONE,
                idNumber: result.ID_NUMBER,
                originalImage: doh1Config.api + result.IMAGE_URL,
                currentImage: azureConfig.serviceHost + 'public/uploads/' + req.files.image[0].filename + '.jpeg'
            });
            return Promise.all([
                rp({
                    method: 'POST',
                    uri: azureConfig.api + azureConfig.detectEndpoint,
                    body: {
                        url: azureConfig.serviceHost + 'public/uploads/' + req.files.image[0].filename + '.jpeg'
                    },
                    headers: {
                        'content-type': 'application/json',
                        'Ocp-Apim-Subscription-Key': azureConfig.key1
                    },
                    json: true
                }),
                rp({
                    method: 'POST',
                    uri: azureConfig.api + azureConfig.detectEndpoint,
                    body: {
                        url: doh1Config.api + result.IMAGE_URL
                    },
                    headers: {
                        'content-type': 'application/json',
                        'Ocp-Apim-Subscription-Key': azureConfig.key1
                    },
                    json: true
                })]);
        })
        .then(function (results) {
            clients.broadcastEmit('log', 'verifying');
            var cameraImageFaceId = results[0][0].faceId;
            var doh1ImageFaceId = results[1][0].faceId;
            return rp({
                method: 'POST',
                uri: azureConfig.api + azureConfig.verifyEndpoint,
                body: {
                    faceId1: cameraImageFaceId,
                    faceId2: doh1ImageFaceId
                },
                headers: {
                    'content-type': 'application/json',
                    'Ocp-Apim-Subscription-Key': azureConfig.key1
                },
                json: true
            });
        })
        .then(function (result) {
            console.log('success');
            clients.broadcastEmit('log', 'success');
            Object.assign(payload, result);
            clients.broadcastEmit('face-match', payload);
            res.status(200).send(result);
        })
        .catch(function (err) {
            clients.broadcastEmit('log', 'err: ' + err.message);
            console.log(err.error);
            console.log(err.message);
            console.log(err.statusCode);
            res.status(err.statusCode).send(err.error.message);
        });
});

module.exports = router;
